#python
from ast import Return
import os
from enum import Enum
from typing import Optional
#Pydantic
from pydantic import EmailStr, BaseModel , Field , SecretStr
#fastapi
from fastapi import FastAPI
from fastapi import Body,Path,Query,Form,File,UploadFile
from fastapi import status, HTTPException
from fastapi import Cookie,Header
from fastapi.responses import FileResponse,HTMLResponse,StreamingResponse
from deta import Deta



app=FastAPI()

class Personaje(BaseModel):
    character_name:str=Field(
        ...,
        min_length=2,
        max_length=50,
        Example="Pedro"
        )
    especie:str


def directory_is_ready():
    os.makedirs(os.getcwd()+"\img",exist_ok=True)
    return os.getcwd()+"\img"


@app.get("/")
def home():
    return {"hello":"Willyreeeex"}

@app.post(path="/me",status_code=status.HTTP_200_OK)
def me(
    name:str=Form(...),
    id:int=Form(...),
    email:EmailStr=Form(...),
    web:str=Form(...),
    age:int=Form(...),
    user_agent:Optional[str]=Header(default=None),
    ads:Optional[str]= Cookie(default=None)
):

    return user_agent


@app.post("/character/new")
def crate_character(personaje:Personaje=Body(...)):
    return personaje

Personajes=["michi","ernesto","zancudini","jorge"]
@app.get("/show/{character_name}")
def show_character(
    character_name: str = Path (...)
):
    if character_name not in Personajes:
        raise HTTPException(
            status_code=status.HTTP_204_NO_CONTENT,
            detail="El personaje no existe"
            )
    return {character_name:" Personaje encontrado"}


@app.post(
    path="/character/upload",
    status_code=status.HTTP_201_CREATED
)
async def upload_picture_character(
    image:UploadFile=File(...)
):
    dir = directory_is_ready()
    print (dir)
    with open(dir+image.filename, "wb") as myfile:
        content = await image.read()
        myfile.write(content)
        myfile.close()
    return{"Success!!LOL"}


@app.get(
    path="/show/picture/{file_name}",
    status_code=status.HTTP_200_OK
)
def show_picture_character(
    file_name:str=Path(...)
):
    dir=directory_is_ready()
    path=dir+file_name

    return FileResponse(path)

@app.get("/pregunta")
def Pregunta():
    return{"El comando PWD imprime la ruta completa del sistema del directorio de trabajo actual en la salida estándar"}